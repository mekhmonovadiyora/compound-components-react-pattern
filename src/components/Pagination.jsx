/* eslint-disable react/prop-types */

export default function Pagination({
  handlePageChange,
  currentPage,
  totalPages,
}) {
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "space-between",
      }}
    >
      <button
        onClick={() => handlePageChange(currentPage - 1)}
        disabled={currentPage === 1}
      >
        Previous
      </button>
      {Array.from({ length: totalPages }).map((page, i) => {
        return (
          <button key={page} onClick={() => handlePageChange(i + 1)}>
            {i + 1}
          </button>
        );
      })}
      <button
        onClick={() => handlePageChange(currentPage + 1)}
        disabled={currentPage === totalPages}
      >
        Next
      </button>
    </div>
  );
}
