/* eslint-disable react/prop-types */

export default function Header({ children }) {
  return (
    <thead
      style={{
        backgroundColor: "#161A30",
      }}
    >
      {children}
    </thead>
  );
}
