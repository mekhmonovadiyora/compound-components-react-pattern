/* eslint-disable react/prop-types */
import Body from "./Body";
import Header from "./Header";
import Row from "./Row";
import Td from "./Td";
import Th from "./Th";

const Table = ({ children }) => {
  return <table>{children}</table>;
};

Table.Header = Header;
Table.Body = Body;
Table.Row = Row;
Table.Th = Th;
Table.Td = Td;

export default Table;
