/* eslint-disable react/prop-types */

export default function Body({ children }) {
  return (
    <tbody
      style={{
        backgroundColor: "#B6BBC4",
        color: "black",
      }}
    >
      {children}
    </tbody>
  );
}
