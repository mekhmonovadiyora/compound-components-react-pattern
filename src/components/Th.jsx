/* eslint-disable react/prop-types */

export default function Th({ children }) {
  return (
    <th
      style={{
        padding: "10px 20px",
      }}
    >
      {children}
    </th>
  );
}
