/* eslint-disable react/prop-types */

export default function Td({ children }) {
  return (
    <td
      style={{
        padding: "10px 20px",
      }}
    >
      {children}
    </td>
  );
}
