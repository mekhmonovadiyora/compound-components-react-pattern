/* eslint-disable react/prop-types */

export default function Footer({ children }) {
  return <tfoot>{children}</tfoot>;
}
