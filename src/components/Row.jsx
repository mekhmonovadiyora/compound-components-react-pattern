/* eslint-disable react/prop-types */

export default function Row({ children }) {
  return <tr>{children}</tr>;
}
