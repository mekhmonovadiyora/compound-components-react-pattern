import { useCallback, useState } from "react";

const itemsPerPage = 3;

export default function usePagination(data) {
  const [currentPage, setCurrentPage] = useState(1);
  const totalPages = Math.ceil(data?.length / itemsPerPage);
  const startIdx = (currentPage - 1) * itemsPerPage;
  const endIdx = startIdx + itemsPerPage;

  const currentItems = data.slice(startIdx, endIdx);

  const handlePageChange = (newPage) => {
    setCurrentPage(newPage);
  };

  const getPaginationProps = useCallback(
    (props) => ({
      totalPages,
      handlePageChange,
      ...props,
    }),
    [totalPages]
  );

  return {
    currentPage,
    handlePageChange,
    totalPages,
    currentItems,
    getPaginationProps,
  };
}
