import "./App.css";

import Pagination from "./components/Pagination";
import Table from "./components/Table";
import usePagination from "./hooks/usePagination";

const bestUniversities = [
  {
    name: "Massachusetts Institute of Technology (MIT)",
    country: "United States",
    ranking: 1,
    tuition: "$50,000",
    acceptanceRate: "6%",
    programs: ["Engineering", "Computer Science", "Physics"],
    campusSize: "168 acres",
  },
  {
    name: "Stanford University",
    country: "United States",
    ranking: 2,
    tuition: "$55,000",
    acceptanceRate: "4%",
    programs: ["Computer Science", "Business", "Biology"],
    campusSize: "8,180 acres",
  },
  {
    name: "Harvard University",
    country: "United States",
    ranking: 3,
    tuition: "$51,000",
    acceptanceRate: "5%",
    programs: ["Law", "Economics", "Medicine"],
    campusSize: "5,076 acres",
  },
  {
    name: "California Institute of Technology (Caltech)",
    country: "United States",
    ranking: 4,
    tuition: "$52,000",
    acceptanceRate: "6%",
    programs: ["Physics", "Chemistry", "Astrophysics"],
    campusSize: "124 acres",
  },
  {
    name: "University of Oxford",
    country: "United Kingdom",
    ranking: 5,
    tuition: "£9,250 (UK/EU), £24,750 (International)",
    acceptanceRate: "17%",
    programs: ["Philosophy", "History", "Economics"],
    campusSize: "Unknown",
  },
  {
    name: "ETH Zurich - Swiss Federal Institute of Technology",
    country: "Switzerland",
    ranking: 6,
    tuition: "CHF 1,298 per semester",
    acceptanceRate: "NA",
    programs: ["Computer Science", "Mechanical Engineering", "Physics"],
    campusSize: "Unknown",
  },
  {
    name: "University of Cambridge",
    country: "United Kingdom",
    ranking: 7,
    tuition: "£9,250 (UK/EU), £24,750 (International)",
    acceptanceRate: "21%",
    programs: ["Mathematics", "Engineering", "English Literature"],
    campusSize: "Unknown",
  },
  {
    name: "Princeton University",
    country: "United States",
    ranking: 8,
    tuition: "$52,800",
    acceptanceRate: "5%",
    programs: ["Economics", "History", "Computer Science"],
    campusSize: "500 acres",
  },
  {
    name: "University of Chicago",
    country: "United States",
    ranking: 9,
    tuition: "$59,298",
    acceptanceRate: "7%",
    programs: ["Economics", "Political Science", "Computer Science"],
    campusSize: "217 acres",
  },
  {
    name: "Imperial College London",
    country: "United Kingdom",
    ranking: 10,
    tuition: "£9,250 (UK/EU), £29,250 (International)",
    acceptanceRate: "15%",
    programs: ["Physics", "Mechanical Engineering", "Medicine"],
    campusSize: "Unknown",
  },
];

function App() {
  const { currentItems, totalPages, currentPage, handlePageChange } =
    usePagination(bestUniversities);

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        gap: "20px",
      }}
    >
      <Table>
        <Table.Header>
          <Table.Row>
            <Table.Th>College name</Table.Th>
            <Table.Th>Location</Table.Th>
            <Table.Th>Ranking</Table.Th>
            <Table.Th>Tuition</Table.Th>
            <Table.Th>Acceptance rate</Table.Th>
            <Table.Th>Programs</Table.Th>
            <Table.Th>Campus size</Table.Th>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {currentItems.map((uni) => (
            <Table.Row key={uni.name}>
              {Object.keys(uni).map((key) => (
                <Table.Td key={key}>{uni[key]}</Table.Td>
              ))}
            </Table.Row>
          ))}
        </Table.Body>
      </Table>

      <Pagination
        totalPages={totalPages}
        currentPage={currentPage}
        handlePageChange={handlePageChange}
      />
    </div>
  );
}

export default App;
